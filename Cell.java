//*******************************************************
// Cell.java
// created by Sawada Tatsuki on 2018/06/05.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* セルのオブジェクト */

import java.util.*;

public class Cell extends AbstractCell {
	public static int col = 92; //行の数 (縦のセル数)
	public static int row = 128; //列の数 (横のセル数)

	public State state; //DEAD or ALIVE
	private State nextState; //DEAD or ALIVE
	
    //コンストラクタ
    public Cell(Simulator simulator, int idI, int idJ) {
		super(simulator, idI, idJ); //コンストラクタを継承
		size(col, row); //格子全体の大きさを設定
		init(); //初期化
    }
	
	//初期化
	public void init() {
		state = rnd.nextBoolean() ? State.DEAD : State.ALIVE; //ランダムな状態

		//壁は DEAD
		if(idI == 0 || idI == col - 1 || idJ == 0 || idJ == row - 1) {
			state = State.DEAD;
		}
	}

	private ArrayList<Cell> neighbors; //近傍の集合
	//観測フェーズ
	public void observe() {
		neighbors = get8Neighbors(simulator.cell); //近傍を観測
	}
	
	private int[] b = {3}; //出生ルール
	private int[] s = {2,3}; //生存ルール
	//状態更新フェーズ
	public void interact() {
		nextState = rule(b, s); //次の状態を取得
	}
	
	//状態更新フェーズ
	public void update() {		
		state = nextState;
	}

	//ルールに基づいて次の状態を返す
	public State rule(int[] b, int[] s) {
		Iterator<Cell> iter = neighbors.iterator(); //リストのイテレータ
		
		int numAlive = 0; //生存セル数
		while(iter.hasNext()) {
			Cell neighbor = iter.next(); //近傍セル
			if(neighbor.state == State.ALIVE) {
				numAlive++; //生存セル数をカウント
			}
		}

		State nextState = State.DEAD; //次の状態

		switch(state) {
		case DEAD: //出生条件
			int numB = b.length;
			for(int i = 0; i < numB; i++) {
				if(b[i] == numAlive) {
					nextState = State.ALIVE;
					break;
				}
			}
			break;
		case ALIVE: //生存条件
			int numS = s.length;
			for(int i = 0; i < numS; i++) {
				if(s[i] == numAlive) {
					nextState = State.ALIVE;
				}
			}
			break;
		default:
			System.exit(1);
		}
		
		return nextState;
	}
}
