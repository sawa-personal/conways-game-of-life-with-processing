//*******************************************************
// Edge.java
// created by Sawada Tatsuki on 2018/06/05.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 境界のタイプの列挙型 */

public enum Edge {
	NORMAL, //普通
	WALLED, //壁あり
	TOROIDAL, //トーラス型
}
