//*******************************************************
// Main.java
// created by Sawada Tatsuki on 2018/01/11.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* Processingによる描画処理 */

import processing.core.*;

public class Main extends Grid2D {
	private int row = simulator.row; //列の数 (横のセル数)
	private int col = simulator.col; //行の数 (縦のセル数)
	
    public void settings() {
		setCellSize(8, 8); //セルの大きさを設定
		size(row, col); //ウィンドウサイズを指定
		setTerminate(-1); //終了ステップ数を設定
		doesSave(false); //動画を保存
    }

    private State state[][]; //状態
	
	//描画処理
    public void controller() {
		frameRate(16); //描画レート
		//overlook(); //俯瞰		
		t = simulator.getStep(); //経過ステップ数を取得
		background(255);//背景色を指定．画面をリセットする役割もある
		
		state = new State[col][row];
		for(int i = 0; i < col; i++) {
			for(int j = 0; j < row; j++) {
				state[i][j] = simulator.cell[i+1][j+1].state;
			}
		}
		
		for(int i = 0; i < col; i++) {
			for(int j = 0; j < row; j++) {
				if(state[i][j] == State.ALIVE) {
					fill(240);
				} else {
					fill(32);
				}
				cell(i, j);
			}			
		}
		
		/*
		fill(40);
		showCountLabel(t); //カウントラベルを表示
		*/
    }
	
    public static void main(String args[]) {
		PApplet.main("Main"); //Mainクラスを呼び出してProcessingを起動
    }
}
